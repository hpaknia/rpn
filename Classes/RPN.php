<?php

namespace Classes;

/**
 * This class provide necessary methods to calculate out put value of a reversed polish notation expression
 *
 * @author hassan.paknia
 */
class RPN {

    /**
     * precision holder of class
     * @var int
     */
    private $precision;

    /**
     * internal stacked data
     * @var array
     */
    private $stack;

    /**
     * last error happened
     * @var string
     */
    private $error;

    /**
     * output calculated by process method
     * @var float
     */
    private $result;

    /**
     * allowed characters in a valid RPN
     * @var array
     */
    private $allowedChars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '+', '-', '*', '/', '^', ' '];

    /**
     * main constructor
     * @param int $precision
     */
    public function __construct($precision = 3) {
        $this->precision = $precision;
    }

    /**
     * precision setter
     * @param int $precision
     */
    public function setPrecision($precision) {
        $this->precision = $precision;
    }

    /**
     * precision getter
     * @return int
     */
    public function getPrecision() {
        return $this->precision;
    }

    /**
     * expression getter
     * @return string
     */
    public function getExpression() {
        return $this->expression;
    }

    /**
     * result getter
     * @return float
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * last error getter
     * @return string
     */
    public function getError() {
        return $this->error;
    }

    /**
     * receives an RPN expression and calculates the result
     * output will be accessible by getResult method
     * in case of successful calculation true is returned, false otherwise
     * @param string $expression
     * @return bool
     */
    public function process($expression) {
        try {
            // prepare
            $this->init($expression);
            // real process
            $this->parse();
            return true;
        } catch (\Exception $ex) {
            // an exception happened, setting the error
            $this->error = $ex->getMessage();
            return false;
        }
    }

    /**
     * resets class variables and checks the expression
     * @param type $expression
     * @throws \Exception
     */
    private function init($expression) {
        $this->error      = '';
        $this->stack      = [];
        $expression       = trim($expression);
        $this->expression = $expression;
        $remaining        = str_replace($this->allowedChars, '', $expression);
        // expression contains invalid chars
        if ($remaining != '') {
            throw new \Exception('Input expression contains invalid character(s): "' . $remaining . '".');
        }
    }

    private function parse() {
        // convert string to char
        $chars = str_split($this->expression);
        // next number found
        $next  = '';
        foreach ($chars as $char) {
            switch ($char) {
                // operators
                case '+':
                case '-':
                case '*':
                case '/':
                case '^':
                    // process operator
                    if (count($this->stack) < 2) {
                        throw new \Exception('Expression is not a valid RPN!');
                    } else {
                        // process operator
                        $this->handle($char);
                    }
                    break;
                case ' ':
                    if ($next != '') {
                        // a new number is found
                        array_push($this->stack, floatval($next));
                        $next = '';
                    } else {
                        continue;
                    }
                    break;
                // numeric
                default :
                    $next .= $char;
                    break;
            }
        }
        if (count($this->stack) != 1) {
            throw new \Exception('Expression is not valid or something went wrong!');
        }
        $this->result = number_format(array_pop($this->stack), $this->precision);
    }

    /**
     * calculates input operator using 2 top elements of stack as operands
     * @param type $operator
     */
    public function handle($operator) {
        $second = array_pop($this->stack);
        $first  = array_pop($this->stack);
        switch ($operator) {
            case '+':
                array_push($this->stack, $first + $second);
                break;
            case '-':
                array_push($this->stack, $first - $second);
                break;
            case '*':
                array_push($this->stack, $first * $second);
                break;
            case '/':
                array_push($this->stack, $first / $second);
                break;
            case '^':
                array_push($this->stack, pow($first, $second));
                break;
        }
    }

}
