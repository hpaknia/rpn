<?php

use PHPUnit\Framework\TestCase;
use Classes\RPN;

class StackTest extends TestCase {

    private $inputHandler;

    public function setUp() {
        parent::setUp();
        // opening input file
        $this->inputHandler = fopen(__DIR__ . '/inputs.txt', 'r');
    }

    public function testInputs() {
        echo PHP_EOL;
        // 
        $rpn        = new RPN(4);
        // read expression line
        while (($expression = fgets($this->inputHandler)) !== false) {
            if (!$rpn->process($expression)) {
                echo $rpn->getError() . ' → "' . $rpn->getExpression() . '"' . PHP_EOL;
                fgets($this->inputHandler);
                continue;
            } else {
                $actual = $rpn->getResult();

                // read expected output line
                $expected = number_format(floatval(fgets($this->inputHandler)), $rpn->getPrecision());

                // assert calculated value
                echo 'Asserting "' . $rpn->getExpression() . '" is equal to ' . $expected . PHP_EOL;
                $this->assertEquals($expected, $actual);
            }
        }
    }

    public function testPushAndPop() {
        $stack = [];
        $this->assertEquals(0, count($stack));

        array_push($stack, 'foo');
        $this->assertEquals('foo', $stack[count($stack) - 1]);
        $this->assertEquals(1, count($stack));

        $this->assertEquals('foo', array_pop($stack));
        $this->assertEquals(0, count($stack));
    }

}

?>